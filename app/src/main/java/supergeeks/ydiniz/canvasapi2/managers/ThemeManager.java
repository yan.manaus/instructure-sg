/*
 * Copyright (C) 2017 - present Instructure, Inc.
 *
 *     Licensed under the Apache License, Version 2.0 (the "License");
 *     you may not use this file except in compliance with the License.
 *     You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *     Unless required by applicable law or agreed to in writing, software
 *     distributed under the License is distributed on an "AS IS" BASIS,
 *     WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *     See the License for the specific language governing permissions and
 *     limitations under the License.
 *
 */

package supergeeks.ydiniz.canvasapi2.managers;

import supergeeks.ydiniz.canvasapi2.StatusCallback;
import supergeeks.ydiniz.canvasapi2.apis.ThemeAPI;
import supergeeks.ydiniz.canvasapi2.builders.RestBuilder;
import supergeeks.ydiniz.canvasapi2.builders.RestParams;
import supergeeks.ydiniz.canvasapi2.models.CanvasTheme;
import supergeeks.ydiniz.canvasapi2.tests.ThemeManager_Test;


public class ThemeManager extends BaseManager {

    private static boolean mTesting = false;

    public static void getTheme(final StatusCallback<CanvasTheme> callback, boolean forceNetwork) {

        if(mTesting || isTesting()) {
            ThemeManager_Test.getTheme(callback);
        } else {

            RestBuilder adapter = new RestBuilder(callback);
            RestParams params = new RestParams.Builder()
                    .withForceReadFromCache(!forceNetwork)
                    .withForceReadFromNetwork(forceNetwork)
                    .build();

            ThemeAPI.getTheme(adapter, callback, params);
        }
    }
}
